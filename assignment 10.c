#include<stdio.h>
int main()
{
    int r,c,arr[20][20],i,j;
    printf("Enter the maximum number of rows:\n");
    scanf("%d",&r);
    printf("Enter the maximum number of columns:\n");
    scanf("%d",&c);
    printf("Enter the elements:\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            scanf("%d",&arr[i][j]);
        }
    }
    printf("The matrix entered:\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d ",arr[i][j]);
        }
        printf("\n");
    }
    return 0;
}
