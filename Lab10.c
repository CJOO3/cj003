#include<stdio.h>
//arthmetic operations using pointers
int main()
{
 int *a,*b, num1, num2;
 float q;
 printf("Enter the first number: \n");
 scanf("%d", &num1);
 printf("Enter the second number:\n");
 scanf("%d",&num2);
 a=&num1;
 b=&num2;
 q=(*a)/(*b);
 printf("\nSum of the given two expressions is: %d",*a+*b);
 printf("\nDifference of the given two expressions is: %d",*a-*b);
 printf("\nProduct of the given two expressions is: %d",(*a)*(*b));
 printf("\nQuotient of the given two expressions is: %f",q);
return 0;
}